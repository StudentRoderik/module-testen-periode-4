﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;

namespace WindesheimAD2021AutoVerzekeringsPremie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(@"  _____          _____   _____ _    _ _____  ______ ");
            Console.WriteLine(@" / ____|   /\   |  __ \ / ____| |  | |  __ \|  ____|");
            Console.WriteLine(@"| |       /  \  | |__) | (___ | |  | | |__) | |__   ");
            Console.WriteLine(@"| |      / /\ \ |  _  / \___ \| |  | |  _  /|  __|  ");
            Console.WriteLine(@"| |____ / ____ \| | \ \ ____) | |__| | | \ \| |____ ");
            Console.WriteLine(@" \_____/_/    \_\_|  \_\_____/ \____/|_|  \_\______|");
            Console.WriteLine(@"~~~ Welcome to the CarSure insurance calculator! ~~~" + "\r\n\r\n");
            Console.ResetColor();
            var startInputLine = Console.CursorTop;

            PolicyHolder policyholder = GetPolicyHolderData();
            ResetConsoleState(startInputLine);

            Vehicle vehicle = GetVehicleData();
            ResetConsoleState(startInputLine);

            InsuranceCoverage coverage = GetCoverage();
            ResetConsoleState(startInputLine);


            DisplayPremium(policyholder, vehicle, coverage);
        }

        
        private static PolicyHolder GetPolicyHolderData()
        {
            Console.WriteLine("Please provide the policy holder's details...\r\n");
            Console.WriteLine("What is the policy holder's age?");
            var lineNumber = Console.CursorTop;
            bool validAge;
            string policyHolderAgeInString;

            do
            {
                policyHolderAgeInString = Console.ReadLine();
                validAge = InputValidator.ValidatePolicyHolder.IsPolicyHolderAgeValid(policyHolderAgeInString);

                if (!validAge)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is an invalid age.", policyHolderAgeInString);
                    Console.ResetColor();
                    Console.WriteLine("\n\nPlease enter a valid age");
                }


            } while (!validAge);
            int age = int.Parse(policyHolderAgeInString);

            Console.WriteLine("\r\nWhen was the policy holder's driving license acquired? (dd-MM-yyyy)"); 
            lineNumber = Console.CursorTop;

            bool validLicenseStartDate;
            string licenseStartDate;

            do
            {
                licenseStartDate = Console.ReadLine();
                validLicenseStartDate = InputValidator.ValidatePolicyHolder.IsLicenseStartDateValid(licenseStartDate);

                if (!validLicenseStartDate)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is an invalid license start date.", licenseStartDate);
                    Console.ResetColor();
                    Console.WriteLine("\n\nPlease enter a valid license start date. The date format must be 'd-m-yyyy'. \n\n The date cannot lie in the future and cannot be older than 10 years");
                }


            } while (!validLicenseStartDate);

            

            Console.WriteLine("\r\nPlease provide the 4 digits of the policy holder's postal code (e.g. 1234)");
            lineNumber = Console.CursorTop;

            bool validPostalCode;
            string postalCodeInString;

            do
            {
                postalCodeInString = Console.ReadLine();
                validPostalCode = InputValidator.ValidatePolicyHolder.IsPostalCodeValid(postalCodeInString);

                if (!validPostalCode)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is an invalid postal code", postalCodeInString);
                    Console.ResetColor();
                    Console.WriteLine("\n\nPlease enter a valid postal code.\n\n The postal code must consist of 4 digits and cannot start with a ");
                }


            } while (!validPostalCode);

            int postalCode = int.Parse(postalCodeInString);

            Console.WriteLine("\r\nPlease provide the number of No-Claim years:");
            lineNumber = Console.CursorTop;

            bool validNoClaimYears;
            string noClaimYersInString;

            do
            {
                noClaimYersInString = Console.ReadLine();
                validNoClaimYears = InputValidator.ValidatePolicyHolder.IsPostalCodeValid(noClaimYersInString);

                if (!validNoClaimYears)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is an invalid number of no claim years", noClaimYersInString);
                    Console.ResetColor();
                    Console.WriteLine("\n\nPlease enter a valid number of no claim years.\n\n The number of no claim years cannot be a negative number");
                }


            } while (!validNoClaimYears);


            int noClaimYears = int.Parse(noClaimYersInString);

            return new PolicyHolder(age, licenseStartDate, postalCode, noClaimYears);
        }

        private static Vehicle GetVehicleData()
        {
            Console.WriteLine("Please provide the vehicle details...");
            Console.WriteLine("What is the vehicle's construction year?");
            var lineNumber = Console.CursorTop;

            bool validConstructionYear;
            string constructionYearInString;

            do
            {
                constructionYearInString = Console.ReadLine();
                validConstructionYear =
                    InputValidator.ValidateVehicle.IsConstructionYearValid(constructionYearInString);

                if (!validConstructionYear)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is not a valid construction year.",constructionYearInString);
                    Console.ResetColor();
                    Console.WriteLine("Please enter a valid construction year. \n\n The year can only contain digits, cannot lie in the future and cannot lie more than 50 years ago");

                }
            } while (!validConstructionYear);

            int constructionYr = int.Parse(constructionYearInString);

            Console.WriteLine("What is the vehicle's value in euro's?");
            lineNumber = Console.CursorTop;

            bool validValueInEuros;
            string valueInEurosInString;

            do
            {
                valueInEurosInString = Console.ReadLine();
                validValueInEuros = InputValidator.ValidateVehicle.IsValueValid(valueInEurosInString);

                if (!validValueInEuros)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is not a valid value.", valueInEurosInString);
                    Console.ResetColor();
                    Console.WriteLine("Please enter a valid value. \n\n The value can only contain digits and must be greater than 0");
                }

            } while (!validValueInEuros);


            int value = int.Parse(valueInEurosInString);

            Console.WriteLine("What is the vehicle's power in Kw");
            lineNumber = Console.CursorTop;

            bool validPowerInKw;
            string powerInKwInString;

            do
            {
                powerInKwInString = Console.ReadLine();
                validPowerInKw = InputValidator.ValidateVehicle.IsPowerInKwValid(powerInKwInString);

                if (!validPowerInKw)
                {
                    ResetConsoleState(lineNumber);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} is not a valid power in Kw.", powerInKwInString);
                    Console.ResetColor();
                    Console.WriteLine("Please enter a valid power in Kw. \n\nThe value can only contain digits and must be greater than 0");
                }
            } while (!validPowerInKw);

            int powerInKw = int.Parse(powerInKwInString);

            return new Vehicle(powerInKw, value, constructionYr);
        }


        private static InsuranceCoverage GetCoverage()
        {
            Console.WriteLine("Please provide the desired coverage:");
            Console.WriteLine("1 - WA");
            Console.WriteLine("2 - WA +");
            Console.WriteLine("3 - All Risk");
            char coverageInput = Console.ReadKey(true).KeyChar;

            switch (coverageInput)
            {
                case '1':
                    return InsuranceCoverage.WA;
                case '2':
                    return InsuranceCoverage.WA_PLUS;
                case '3':
                    return InsuranceCoverage.ALL_RISK;               
                default:
                    DisplayError("Unknown vehicle type. Please Try again!");
                    return GetCoverage();
            }
        }

        private static void DisplayPremium(PolicyHolder policyholder, Vehicle vehicle, InsuranceCoverage coverage)
        {
            PremiumCalculation calculation = new(vehicle, policyholder, coverage);

            Console.WriteLine("Please provide the desired premium period:");
            Console.WriteLine("1 - Month");
            Console.WriteLine("2 - year");
            char periodInput = Console.ReadKey(true).KeyChar;
            
            PaymentPeriod p;
            string timeunit;

            switch (periodInput)
            {                
                case '2':
                    p = PaymentPeriod.YEAR;
                    timeunit = "year";
                    break;
                default:
                    p = PaymentPeriod.MONTH;
                    timeunit = "month";
                    break;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Your premium is {0} per {1}", calculation.PremiumPaymentAmount(p), timeunit); 
            Console.WriteLine("------------------------------------------");
            Console.ResetColor();
            Console.WriteLine("\r\nPress any key to quit.");
            Console.ReadKey(true);
        }

        private static void DisplayError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[ERROR] {0}", message);
            Console.ResetColor();
        }

        public static void ResetConsoleState(int lineToClearFrom)
        {
            for (var line = Console.CursorTop; line >= lineToClearFrom; line--)
            {
                Console.SetCursorPosition(0, line);
                Console.Write(new string(' ', Console.WindowWidth));
            }
            Console.SetCursorPosition(0, lineToClearFrom);
        }
    }
}
