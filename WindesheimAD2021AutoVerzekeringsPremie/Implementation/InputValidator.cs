﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    class InputValidator
    {

        public static class ValidatePolicyHolder
        {
            public static bool IsPolicyHolderAgeValid(string policyHolderAge)
            {
                if (!int.TryParse(policyHolderAge, out var result))
                    return false;

                return result > 0 && result < 110;

            }

            public static bool IsLicenseStartDateValid(string licenseStartDate)
            {
                if (!DateTime.TryParseExact(licenseStartDate, "d-M-yyyy", new CultureInfo("nl-NL"), DateTimeStyles.None,
                    out var result))
                    return false;

                return result <= DateTime.Now && result > DateTime.Now.AddYears(-10);

            }


            public static bool IsPostalCodeValid(string postalCode)
            {
                if (!int.TryParse(postalCode, out var result))
                    return false;

                return result >= 1000 && result <= 9999;
            }

            public static bool IsNoClaimYearsValid(string noClaimYears)
            {
                if (!int.TryParse(noClaimYears, out var result))
                    return false;

                return (result >= 0);
            }
        }

        public static class ValidateVehicle
        {

            public static bool IsPowerInKwValid(string powerInKw)
            {
                if (!int.TryParse(powerInKw, out var result))
                    return false;

                return result > 0;

            }

            public static bool IsValueValid(string valueInEuros)
            {
                if (!int.TryParse(valueInEuros, out var result))
                    return false;

                return result > 0;

            }

            public static bool IsConstructionYearValid(string constructionYear)
            {
                if (!int.TryParse(constructionYear, out var result))
                    return false;

                return result >= DateTime.Now.AddYears(-50).Year && result <= DateTime.Now.Year;
            }
        }

        
    }
}
