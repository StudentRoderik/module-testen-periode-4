﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace UnitTestsAutoVerzekeringsPremie
{
    public class VehicleTests
    {

        [Theory]
        [InlineData(50, 15000, 0)]
        [InlineData(50, 20000, 0)]
        [InlineData(80, 10000, 1)]
        public void TestIfAgeIs0WhenConstructionYearIsGreaterThanCurrentYear(int PowerInKw, int ValueInEuros, int ExpectedAge)
        {
                //Arrange
                var constructionYear = DateTime.Now.Year - ExpectedAge;
                


                 //Act
                var vehicle1 = new Vehicle(PowerInKw, ValueInEuros, constructionYear);
                var actualAge = vehicle1.Age;


                //Assert

                Assert.Equal(ExpectedAge, actualAge);


        }

        [Theory]
        [InlineData(120, 13000, 2018, 3)]
        [InlineData(230, 60000, 2019, 2)]
        [InlineData(200, 45000, 2020, 1)]
        [InlineData(150, 50000, 2021, 0)]
        [InlineData(220, 55000, 2022, 0)]

        public void TestIfAgeIsCurrentYearMinusConstructionYear(int PowerInKw, int ValueInEuros, int ConstructionYear, int ExpectedAge)
        {
            //Arrange


            //Act
            var vehicle2 = new Vehicle(PowerInKw, ValueInEuros, ConstructionYear);
            var actualAge = vehicle2.Age;

            //Assert

            Assert.Equal(ExpectedAge, actualAge);


        }

    }
}
