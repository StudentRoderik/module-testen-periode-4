using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace UnitTestsAutoVerzekeringsPremie
{
    public class PremiumCalculationTests
    {
        [Fact]
       public void TestIf15PercentIsAddedToPremiumWhenDriverIsUnder23YearsOld()
        {

            var vehicle1 = new Mock<IVehicle>();
            vehicle1.Setup(p => p.PowerInKw).Returns(100);
            vehicle1.Setup(p => p.ValueInEuros).Returns(20000);
            vehicle1.Setup(p => p.Age).Returns(11);

            /*var vehicle1 = new Vehicle(100, 20000, 2010);*/

            var policyHolder1 = new Mock<IPolicyHolder>();
            policyHolder1.Setup(p => p.Age).Returns(22);
            policyHolder1.Setup(p => p.PostalCode).Returns(8888);
            policyHolder1.Setup(p => p.LicenseAge).Returns(11);
            policyHolder1.Setup(p => p.NoClaimYears).Returns(4);

            var premiumAge22 = new PremiumCalculation(vehicle1.Object, policyHolder1.Object, InsuranceCoverage.WA);
            

           /* var policyHolder1 = new PolicyHolder(Age, "22-05-2010", 8888, 4);*/

            
            //Act
           /* var premiumCalculation = new PremiumCalculation(vehicle1, policyHolder1, InsuranceCoverage.WA);

            var actualPremium = premiumCalculation.PremiumAmountPerYear;*/


            //Assert 

            Assert.Equal((209 / 3D) * 1.15D, premiumAge22.PremiumAmountPerYear);
            
        }

       [Fact]
       public void TestIfNoPercentageIsAddedToPremiumWhenDriverIsOver23YearsOld()
       {

           var vehicle1 = new Mock<IVehicle>();
           vehicle1.Setup(p => p.PowerInKw).Returns(100);
           vehicle1.Setup(p => p.ValueInEuros).Returns(20000);
           vehicle1.Setup(p => p.Age).Returns(11);

           /*var vehicle1 = new Vehicle(100, 20000, 2010);*/

           var policyHolder1 = new Mock<IPolicyHolder>();
           policyHolder1.Setup(p => p.Age).Returns(23);
           policyHolder1.Setup(p => p.PostalCode).Returns(8888);
           policyHolder1.Setup(p => p.LicenseAge).Returns(11);
           policyHolder1.Setup(p => p.NoClaimYears).Returns(4);

           var premiumAge22 = new PremiumCalculation(vehicle1.Object, policyHolder1.Object, InsuranceCoverage.WA);


           /* var policyHolder1 = new PolicyHolder(Age, "22-05-2010", 8888, 4);*/


           //Act
           /* var premiumCalculation = new PremiumCalculation(vehicle1, policyHolder1, InsuranceCoverage.WA);

            var actualPremium = premiumCalculation.PremiumAmountPerYear;*/


           //Assert 

           Assert.Equal((209 / 3D), premiumAge22.PremiumAmountPerYear);

       }



        [Theory]
        [InlineData(5, 0, (209 / 3D))]
        [InlineData(5, 1, (209 / 3D))]
        [InlineData(5, -1, (209 / 3D * 1.15D))]
        [InlineData(6,0, (209 / 3D))]
        
            public void TestIf15PercentIsAddedToPremiumWhenDriversLicenseAgeIsUnder5YearsOld(
            int DriverLicenseAgeInYears, int DriversLicenseAgeInDays, double expectedOutcome)
        {
            var driverLicenseStartDate = DateTime.Today.AddYears(-DriverLicenseAgeInYears)
                .AddDays(-DriversLicenseAgeInDays).ToString();

            var vehicle1 = new Vehicle(100, 20000, 2010);

            var policyHolder1 = new PolicyHolder(23, driverLicenseStartDate, 8888, 4);

            
            //Act
            var premiumCalculation = new PremiumCalculation(vehicle1, policyHolder1, InsuranceCoverage.WA);

            var actualPremium = premiumCalculation.PremiumAmountPerYear;


            //Assert 

            Assert.Equal(expectedOutcome, actualPremium);




        }

        [Theory]
        
        [InlineData(100, 20000, 2010, (209/ 3D) )]

        public void TestIfBasePremiumIsCalculatedRight(int PowerInKw, int ValueInEuros, int ConstructionYear, double ExpectedOutcome)
        {
            
            //Arrange
            var vehicle1 = new Vehicle(PowerInKw, ValueInEuros, ConstructionYear);
           
            //Act
            double actualOutcome = PremiumCalculation.CalculateBasePremium(vehicle1);


           //Assert 
           Assert.Equal(ExpectedOutcome, actualOutcome);
        }

        [Theory]
        [InlineData(100, int.MinValue, 100)]
        [InlineData(100, 0999, 100)]
        [InlineData(100, 1000, 105)]
        [InlineData(100, 1001, 105)]
        [InlineData(100, 3599, 105)]
        [InlineData(100, 3600, 102)]
        [InlineData(100, 3601, 102)]
        [InlineData(100, 4499, 102)]
        [InlineData(100, 4500, 100)]
        [InlineData(100, 4501, 100)]
        [InlineData(100, int.MaxValue, 100)]
        public void TestIfTheRightPercentIsAddedToPremiumBasedOnPostalCode(double premium, int postalCode, double expectedPremium)
        {
          
            var actualPremium = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForPostalCode", BindingFlags.NonPublic | BindingFlags.Static)
                .Invoke(null, new Object[] {premium, postalCode});

            Assert.Equal(expectedPremium, actualPremium);
        }


        [Theory]
        [InlineData(100, 17, 40)]
        [InlineData(100, 18, 35)]
        [InlineData(100, 19, 35)]
        [InlineData(100, int.MaxValue, 35)]
        public void TestIfNoClaimPercentageIs65WhenNoClaimPercentageIsAbove65Percent(double premium, int years, double expectedPremium)
        {
            var actualPremium = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForNoClaimYears", BindingFlags.NonPublic | BindingFlags.Static)
                .Invoke(null, new Object[] { premium, years });

            Assert.Equal(expectedPremium, actualPremium);
        }

        [Theory]
        
        [InlineData(100, 4, 100)]
        [InlineData(100, 5, 100)]
        [InlineData(100, 6, 95 )]

        public void TestIfNoClaimPercentageIs0WhenNoClaimPercentageIsBelow0Percent(double premium, int years, double expectedPremium)
        {
            var actualPremium = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForNoClaimYears", BindingFlags.NonPublic | BindingFlags.Static)
                .Invoke(null, new Object[] { premium, years });

            Assert.Equal(expectedPremium, actualPremium);

        }

        [Fact]

        public void TestIf2AndAHalfPercentageDiscountIsGivenWhenYearIsSelectedAsPaymentPeriod()
        {
            var period = PremiumCalculation.PaymentPeriod.YEAR;
            var vehicle2 = new Vehicle(100, 10000, 2005);
            var policyHolder2 = new PolicyHolder(30, "13-01-2015", 8000, 8);

            var premiumCalculation = new PremiumCalculation(vehicle2, policyHolder2, InsuranceCoverage.WA);


            double expectedPremiumAmount = Math.Round(premiumCalculation.PremiumAmountPerYear * 0.975, 2);

            //Act

            double actualOutcome = premiumCalculation.PremiumPaymentAmount(period);

            Assert.Equal(expectedPremiumAmount, actualOutcome);

        }

        [Fact]
        public void TestIfPremiumIsDivideBy12WhenMonthIsSelectedAsPaymentPeriod()
        {
            var period = PremiumCalculation.PaymentPeriod.MONTH;
            var vehicle3 = new Vehicle(100, 10000, 2005);
            var policyHolder3 = new PolicyHolder(30, "13-01-2015", 8000, 8);

            var premiumCalculation = new PremiumCalculation(vehicle3, policyHolder3, InsuranceCoverage.WA);


            double expectedPremiumAmount = Math.Round(premiumCalculation.PremiumAmountPerYear / 12, 2);

            //Act

            double actualOutcome = premiumCalculation.PremiumPaymentAmount(period);

            Assert.Equal(expectedPremiumAmount, actualOutcome);
        }

        [Theory]
        [InlineData((209 / 3D))]

        public void TestIfNoPercentageIsAddedWhenInsuranceCoverageWAisSelected(double expectedOutcome)
        {
            var vehicle4 = new Vehicle(100, 20000, 2010);

            var policyHolder4 = new PolicyHolder(30, "24-03-2010", 8888, 4);


            //Act
            var premiumCalculation = new PremiumCalculation(vehicle4, policyHolder4, InsuranceCoverage.WA);

            var actualPremium = premiumCalculation.PremiumAmountPerYear;


            //Assert 

            Assert.Equal(expectedOutcome, actualPremium);


        }

        [Theory]

        [InlineData((209 / 3D * 1.2))]

        public void TestIf20PercentIsAddedToPremiumWhenInsuranceCoverageWAPLUSisSelected(double expectedOutcome)
        {
            //Arrange
            var vehicle5 = new Vehicle(100, 20000, 2010);

            var policyHolder5 = new PolicyHolder(65, "17-10-1981", 8888, 4);


            //Act
            var premiumCalculation = new PremiumCalculation(vehicle5, policyHolder5, InsuranceCoverage.WA_PLUS);

            var actualPremium = premiumCalculation.PremiumAmountPerYear;


            //Assert 

            Assert.Equal(expectedOutcome, actualPremium);

        }


        [Theory]

        [InlineData(((209 / 3D) * 2))]

        public void TestIf100PercentIsAddedToPremiumWhenInsuranceCoverageWAALLRISKisSelected(double expectedOutcome)
        {
            //Arrange
            var vehicle5 = new Vehicle(100, 20000, 2010);

            var policyHolder5 = new PolicyHolder(46, "02-11-1990", 8888, 4);


            //Act
            var premiumCalculation = new PremiumCalculation(vehicle5, policyHolder5, InsuranceCoverage.ALL_RISK);

            var actualPremium = premiumCalculation.PremiumAmountPerYear;


            //Assert 

            Assert.Equal(expectedOutcome, actualPremium);

        }
    }
}

       
