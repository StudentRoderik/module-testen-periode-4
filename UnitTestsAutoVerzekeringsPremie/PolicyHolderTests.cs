﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace UnitTestsAutoVerzekeringsPremie
{
    public class PolicyHolderTests
    {
        [Theory]

        [InlineData(0, 8, -1)]
        [InlineData(1, 8, -2)]
        [InlineData(-1, -5, 1)]

        public void TestIfLicenseAgeIsSubtractedByAYearWhenNoFullYearHasElapsed(int years, int months, int expectedLicenseAge)
        {
            var currentDate = DateTime.Today;
            

            //Act

            var actualLicenseAge = typeof(PolicyHolder)
                .GetMethod("AgeByDate", BindingFlags.NonPublic | BindingFlags.Static)
                .Invoke(null, new Object[] { currentDate.AddYears(years).AddMonths(months) });

            //Assert

            Assert.Equal(expectedLicenseAge, actualLicenseAge);
            

        }

        

        [Fact]

        public void TestIfDateStringIsParsedToDateTimeFormat()
        {
            string driverLicenseStartDate = "05-06-2010";

            var expectedDateTime = DateTime.Parse(driverLicenseStartDate, new CultureInfo("nl-NL"),
                DateTimeStyles.NoCurrentDateDefault);

            var actualDateTime = typeof(PolicyHolder)
                .GetMethod("ParseDate", BindingFlags.NonPublic | BindingFlags.Static)
                .Invoke(null, new Object[] { driverLicenseStartDate });

            Assert.Equal(expectedDateTime, actualDateTime);

        }

        

    }
}
